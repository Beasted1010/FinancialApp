
#include <windows.h>
#include <stdio.h>

#include "pixel.h"
#include "appstate.h"
#include "font.h"
#include "common.h"

Bitmap bitmap;
AppState state;

void GetWindowDimensions( HWND window, AppState* state )
{
    RECT clientRect;
    GetClientRect( window, &clientRect );

    state->windowDimensions.width = clientRect.right - clientRect.left;
    state->windowDimensions.height = clientRect.bottom - clientRect.top;
}

void DisplayBitmapToWindow( HDC deviceContext, AppState* state, Bitmap* bitmap, uint16_t x, uint16_t y )
{
    // TODO: Get rid of StretchDIBits, replace with non-Windows specific code
    //this will allow for bitmap->info being eliminated from the bitmap structure
    StretchDIBits( deviceContext, x, y, bitmap->width, bitmap->height,
                   x, y, state->windowDimensions.width, state->windowDimensions.height,
                   bitmap->memory, &bitmap->info, DIB_RGB_COLORS, SRCCOPY );
}

void Paint( HDC deviceContext )
{
    Rect testRect = { state.windowDimensions.width / 2,
                      state.windowDimensions.height / 2,
                      state.windowDimensions.width / 5,
                      state.windowDimensions.height / 5 };
    RGB testCol = {255, 255, 255};
    FillRectangle( &bitmap, &testRect, &testCol );
    RGB testCol2 = {255, 0, 0};
    DrawRectangle( &bitmap, &testRect, &testCol2 );

    Circle circle = { state.windowDimensions.width / 2,
                      state.windowDimensions.height / 2,
                      state.windowDimensions.height / 10 };
    DrawCircle( &bitmap, &circle, &testCol2 );

    Line line = { testRect.x,
                  testRect.y,
                  testRect.x + testRect.w,
                  testRect.y + testRect.h };
    DrawLine( &bitmap, &line, &testCol2 );

    Line l = { testRect.x + testRect.w,
              testRect.y,
              testRect.x,
              testRect.y + testRect.h };
    DrawLine( &bitmap, &l, &testCol2 );

    DisplayBitmapToWindow( deviceContext, &state, &bitmap, 0, 0 );
}

LRESULT CALLBACK WindowProc( HWND window, UINT message, WPARAM wParam, LPARAM lParam )
{
    LRESULT result;

    switch(message)
    {
        case WM_SIZE:
        {
            GetWindowDimensions( window, &state );

            InitializeBitmap( &bitmap, &state.windowDimensions.width,
                                       &state.windowDimensions.height, BLACK );

            // TODO: This should not go here

        } break;
        case WM_SETFONT:
        {
            /*TODO HDC deviceContext = GetDC(window);
            HFONT font = CreateFontWrapper( 20, 20 );
            SelectObject( deviceContext, font );
            SetTextColor( deviceContext, RGB(0,255,0) );
            ReleaseDC(window, deviceContext);*/
        } break;
        case WM_SETCURSOR:
        {
            SetCursor( LoadCursor(0, IDC_ARROW) );
        } break;
        case WM_GETMINMAXINFO:
        {
            MINMAXINFO* minMaxInfo = (MINMAXINFO*) lParam;
            minMaxInfo->ptMinTrackSize.x = state.windowDimensions.minWidth;
            minMaxInfo->ptMinTrackSize.y = state.windowDimensions.minHeight;
        } break;
        case WM_PAINT:
        {
            PAINTSTRUCT paintInfo;
            HDC deviceContext = BeginPaint(window, &paintInfo);

            Paint( deviceContext );

            EndPaint(window, &paintInfo);
        } break;
        case WM_CLOSE:
        {
            printf("WM_CLOSE\n");
            PostQuitMessage(0);
        } break;
        case WM_DESTROY:
        {
            printf("WM_DESTROY\n");
            PostQuitMessage(0);
        } break;
        case WM_QUIT:
        {
            printf("WM_QUIT\n");
        } break;
        default:
        {
            result = DefWindowProc( window, message, wParam, lParam );
        } break;
    }

    return result;
}


int CALLBACK WinMain( HINSTANCE instance, HINSTANCE prevInstance, PSTR args, int showArgs )
{
    WNDCLASS windowClass = {0,0,0,0,0,0,0,0,0,0}; // For Visual Studio friendliness

    windowClass.style = CS_HREDRAW | CS_VREDRAW;

    windowClass.lpfnWndProc = WindowProc;

    windowClass.hInstance = instance;

    //windowClass.hIcon = ;

    // TODO: Create our own cursor
    //windowClass.hCursor = SetCursor( LoadCursor(0, IDC_ARROW) );

    windowClass.lpszClassName = "WindowClass";

    windowClass.lpszMenuName = "WindowMenu";

    if( RegisterClass( &windowClass ) )
    {
        HWND window = CreateWindowEx( 0, windowClass.lpszClassName, "NAME ME",
                                        WS_OVERLAPPEDWINDOW | WS_VISIBLE,
                                        CW_USEDEFAULT, CW_USEDEFAULT, CW_USEDEFAULT, CW_USEDEFAULT,
                                        NULL, NULL, instance, NULL );

        if( window )
        {
            MSG message;
            InitializeAppState( &state, MINWINDOWSIZE_W, MINWINDOWSIZE_H );
            while( state.running )
            {
                while( PeekMessage( &message, NULL, 0, 0, PM_REMOVE ) )
                {
                    if( message.message == WM_QUIT )
                    { state.running = 0; break; }

                    TranslateMessage( &message );
                    DispatchMessage( &message );
                }
                /* TODO: Releasing DC break the app... not havin ReleaseDC breaks the machine...
                HDC deviceContext = GetDC(window);
                HFONT font = CreateFontWrapper( 20, 20 );
                SelectFontWrapper( deviceContext, font );
                SetTextColor( deviceContext, RGB(0,255,0) );
                ReleaseDC(window, deviceContext);
                Rect testRect = { state.windowDimensions.width / 2,
                                  state.windowDimensions.height / 2,
                                  state.windowDimensions.width / 5,
                                  state.windowDimensions.height / 5 };
                DrawTextWrapper( deviceContext, "HELLO", testRect );*/
            }
        }
        else
        {
            // TODO: LOG ERROR
        }
        DestroyCursor( windowClass.hCursor );
        DestroyBitmap( &bitmap );
    }
    else
    {
        // TODO: LOG ERROR
    }
    return 0;
}
