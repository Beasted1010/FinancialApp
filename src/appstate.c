

#include "appstate.h"


void InitializeAppState( AppState* state, uint16_t minWndSizeW, uint16_t minWndSizeH )
{
    state->running = 1;

    state->windowDimensions.minWidth = minWndSizeW;
    state->windowDimensions.minHeight = minWndSizeH;
}
