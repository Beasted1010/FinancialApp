

#include "pixel.h"

static inline void ColorPixel( uint32_t* pixel, const RGB* color )
{
    // BB GG RR xx -> How the memory looks (LITTLE-ENDIAN! So BB is low order byte)
    *pixel = ( (color->r << 16) | (color->g << 8) | color->b );
}

static inline int OutOfMemoryBounds( const Bitmap* bitmap, uint32_t* pixel )
{
    // firstPixel contains the ADDRESS of the first byte in memory and spans the first pixel
    // memorySpan contains the length memory that is our bitmap
    uint32_t firstPixel = (uint32_t) bitmap->memory;
    uint32_t memorySpan = bitmap->memorySpan;

    // TODO: I am unsure how spot-on this is, it may be several 'pixels' off..?
    // Checking to see if our pixel is pointing to something outside of our bitmap range
    if( pixel > (uint32_t*)(firstPixel + memorySpan) || pixel < (uint32_t*)firstPixel )
    { return 1; } // LOG ERROR

    return 0;
}

static inline void ClearBitmap( Bitmap* bitmap )
{
    /* Keeping the below for learning purposes, to understand what memset essentially does
    // Pixel begins by pointing to the first pixel in memory
    uint32_t* pixel = (uint32_t*) bitmap->memory;
    for( int y = 0; y < bitmap->height; y++ )
    {
        for( int x = 0; x < bitmap->width; x++ )
        {
            // Update current value, and then incremenet by 1 pixel (sizeof(pixel) = 32 bits)
            ColorPixel( pixel, color );
            pixel++;
        }
    }*/

    // Below is a much more efficient way of setting the memory
    if( bitmap->clearColor )
    { memset( bitmap->memory, 255, bitmap->memorySpan ); }
    else
    { memset( bitmap->memory, 0, bitmap->memorySpan ); }
}

void DestroyBitmap( Bitmap* bitmap )
{
    VirtualFree( bitmap->memory, 0, MEM_RELEASE );
    bitmap->memory = 0;
    bitmap->width = 0;
    bitmap->height = 0;
    bitmap->pitch = 0;
}

void InitializeBitmap( Bitmap* bitmap, const uint16_t* width,
                        const uint16_t* height, const uint8_t* clearColor )
{
    if( bitmap->memory ) // If there is bitmap memory in use, free it, we will reallocate
    {
        DestroyBitmap( bitmap );
    }

    // TODO: Adjust this to allow for MONOCHROME color?
    bitmap->width = *width;
    bitmap->height = *height;

    bitmap->bytesPerPixel = 4;

    // # of pixels * how many bytes per pixel
    // The number of bytes per row of the bitmap
    // Not incredibly useful since if pixel is an uint32_t* then doing pointer arithmetic will
    //increment pixel by sizeof(pixel) for each specified increment. Ex: pixel += bitmap->width
    bitmap->pitch = bitmap->width * bitmap->bytesPerPixel;

    bitmap->bitmapType = COLOR; // = bitmapType -> was an old parameter

    bitmap->info.bmiHeader.biSize = sizeof(bitmap->info.bmiHeader);
    bitmap->info.bmiHeader.biWidth = bitmap->width;
    bitmap->info.bmiHeader.biHeight = -bitmap->height; // Top-down bitmap
    bitmap->info.bmiHeader.biPlanes = 1;
    bitmap->info.bmiHeader.biBitCount = bitmap->bytesPerPixel * 8;
    bitmap->info.bmiHeader.biCompression = BI_RGB;
    bitmap->info.bmiHeader.biSizeImage = 0;
    bitmap->info.bmiHeader.biXPelsPerMeter = 0;
    bitmap->info.bmiHeader.biYPelsPerMeter = 0;
    bitmap->info.bmiHeader.biClrUsed = 0;
    bitmap->info.bmiHeader.biClrImportant = 0;

    bitmap->memorySpan = (bitmap->width * bitmap->height) * bitmap->bytesPerPixel;
    bitmap->memory = VirtualAlloc( NULL, bitmap->memorySpan, MEM_COMMIT, PAGE_READWRITE );

    ClearBitmap( bitmap );
}

// TODO: startX and startY are unsigned, therefore the check to see if startX is neg is ridiculous
// Perhaps just make these ints...
static inline void DrawHorizontalLine( const Bitmap* bitmap, uint16_t startX, uint16_t startY,
                                                       uint16_t length, const RGB* color )
{
    uint32_t* pixel = (uint32_t*) bitmap->memory;

    // startY * bitmap->width is the amount of pixels we need to move down (in the Y direction)
    // startX is the amount of pixels we need to move right (in the X direction)
    pixel += (startY * bitmap->width) + startX;

    for( int i = 0; i < length; i++ )
    {
        if( OutOfMemoryBounds( bitmap, pixel ) )
        { return; }

        ColorPixel( pixel, color );
        pixel++;
    }
}

static inline void DrawVerticalLine( const Bitmap* bitmap, uint16_t startX, uint16_t startY,
                                                     uint16_t length, const RGB* color )
{
    uint32_t* pixel = (uint32_t*) bitmap->memory;

    // startY * bitmap->width is the amount of pixels we need to move down (in the Y direction)
    // startX is the amount of pixels we need to move right (in the X direction)
    pixel += (startY * bitmap->width) + startX;

    for( int i = 0; i < length; i++ )
    {
        if( OutOfMemoryBounds( bitmap, pixel ) )
        { return; }

        ColorPixel( pixel, color );
        pixel += bitmap->width;
    }
}

void DrawRectangle( const Bitmap* bitmap, const Rect* rect, const RGB* color )
{
    DrawHorizontalLine( bitmap, rect->x, rect->y, rect->w, color );
    DrawHorizontalLine( bitmap, rect->x, rect->y + rect->h, rect->w, color );

    DrawVerticalLine( bitmap, rect->x, rect->y, rect->h, color);
    DrawVerticalLine( bitmap, rect->x + rect->w, rect->y, rect->h, color);
}

void FillRectangle( const Bitmap* bitmap, const Rect* rect, const RGB* color )
{
    for( int i = 0; i <= rect->h; i++ )
    { DrawHorizontalLine( bitmap, rect->x, rect->y + i, rect->w, color); }
}

static inline void NextLinePoint( float slope, const Line* line, int x, int y, int* outX, int* outY )
{
    // We are only interested in the magnitude of the slope
    if( slope < 0 )
    { slope = -slope; }

    float err = y - (slope * x);

    // TODO: Notice the repeating code on each end of the if, this can be optimized by eliminating the else, and
    //possibly eliminating the if statement entirely, combining the checks together. -> ENSURE STILL WORKS
    if( (line->endX > line->startX) && (line->endY > line->startY) )
    {
        if( slope < 1 )
        {
            err -= slope;
            x++;

            if( err < -0.5 )
            { y++; }
        }
        else if( slope >= 1 )
        {
            err += 1;
            y++;

            if( err > 0.5 )
            { x++; }
        }
    }
    else if( (line->endX > line->startX) && (line->endY < line->startY) )
    {
        if( slope < 1 )
        {
            err -= slope;
            x++;

            if( err < -0.5 )
            { y++; }
        }
        else if( slope >= 1 )
        {
            err += 1;
            y++;

            if( err > 0.5 )
            { x++; }
        }
    }
    *outX = x;
    *outY = y;
}

void DrawLine( const Bitmap* bitmap, Line* line, const RGB* color )
{
    if( line->startY == line->endY )
    {
        int length = line->endX - line->startX;

        if( length < 0 )
        { length = -length; }

        DrawHorizontalLine( bitmap, line->startX, line->startY, length, color );

        return;
    }
    else if( line->startX == line->endX )
    {
        int length = line->endY - line->startY;

        if( length < 0 )
        { length = -length; }

        DrawVerticalLine( bitmap, line->startX, line->startY, length, color );

        return;
    }

    // NOTE: Negating (delta)Y due to bitmap being top down and coordinate plane is bottom up
    float slope = (line->startY - line->endY) / (float)(line->endX - line->startX);

    // Scoped to rid unneeded variable later
    // TODO: Why  not just throw temp inside the if block... FACEPALM
    {
        int temp;
        // We want to view our line as if it is always going to the right
        if( line->endX < line->startX )//|| line->endY < line->startY )
        {
            temp = line->startX;
            line->startX = line->endX; // Start where line "ends" for simplicity
            line->endX = temp;

            temp = line->startY;
            line->startY = line->endY; // Start where line "ends" for simplicity
            line->endY = temp;
        }
    }

    int x = 0;
    int y = 0;

    uint32_t* firstPixel = (uint32_t*) bitmap->memory;

    int tempX, tempY;

    int yLoc, xLoc;
    int relativePixelLoc;

    uint32_t* pixel;

    while( x < (line->endX - line->startX)  )
    {
        // If slope is positive we are going UP our bitmap due to how we defined our line's direction (see above)
        if( slope > 0 )
        { yLoc = line->startY - y; }
        // If slope is negative we are going DOWN our bitmap due to how we defined our line's direction (see above)
        else if ( slope < 0 )
        { yLoc = line->startY + y; }

        // We only move RIGHT on our bitmap due to how we defiend our line's direction (see above)
        xLoc = line->startX + x;

        relativePixelLoc = (yLoc * bitmap->width) + xLoc;

        pixel = firstPixel + relativePixelLoc;

        // This is above Bounds check so that the "continue" does not lead to an infinite loop
        NextLinePoint( slope, line, x, y, &tempX, &tempY );
        x = tempX;
        y = tempY;

        if( OutOfMemoryBounds( bitmap, pixel ) )
        { continue; }

        ColorPixel( pixel, color);
    }
}

// Using OUT parameters to prevent having to derefrence constantly to change a value.
// Which probably isn't a big deal, but makes code clean -> Revisit thought to reconsider.
static inline void NextHalfCirclePoint( uint16_t radius, int x, int y, int* outX, int* outY )
{
    float err = (radius * radius) -  (x * x) - (y * y);

    if( x > y && y >= 0 )
    {
        err -= (2*y) + 1;
        y++;

        if( err < -0.5 )
        { x--; }
    }
    else if( x <= y && x >= 0 )
    {
        err += (2*x) - 1;
        x--;

        if( err > 0.5 )
        { y++; }
    }
    else if( (-x) <= y && x < 0 )
    {
        err += (2*x) - 1;
        x--;

        if( err < -0.5 )
        { y--; }
    }
    else if( (-x) > y && x < 0 )
    {
        err += (2*y) - 1;
        y--;

        if( err > 0.5 )
        { x--; }
    }

    *outX = x;
    *outY = y;
}

void DrawCircle( const Bitmap* bitmap, const Circle* circle, const RGB* color )
{
    int x = circle->radius;
    int y = 0;

    // Point to first byte in bitmap
    uint32_t* firstPixel = (uint32_t*) bitmap->memory;

    int tempX, tempY;

    int yLoc, xLoc;
    // 32 bits is an integer, which is the size of our pixel
    int relativePixelLoc;

    uint32_t* pixel;

    while( x >= 0 )
    {
        // NOTE: I BROKE THE 32 BIT BOUNDARY RULE!!!! AND LIKELY POINTING OUTSIDE OF BITMAP MEMORY!!
        // What I'm doing below is taking what was pointing to memory and having it point to garbage
        //pixel = (uint32_t*) ( ( yLoc * bitmap->width ) + xLoc ); -> LESSON LEARNED (:

        // We essentially take our pixel in first quandrant and then use symmetry for the others
        // This here to take advantage of symmetry
        for( int i = 0; i < 2; i++ )
        {
            // Simulate an X-Y Plane in our bitmap (with traditional Pos. Y = up, Pos. X = right)
            // We want to go UP our bitmap if posiitve, if neg go down our bitmap
            if( i )
            { yLoc = circle->centerY - y; }
            // Similar to above but this case is for the other end of the circle
            else
            { yLoc = circle->centerY + y; }

            for( int j = 0; j < 2; j++ )
            {
                // We want to go RIGHT of our bitmap if positive, if neg. go left of bitmap
                if( j )
                { xLoc = circle->centerX + x; }
                // Similar to above but this case is for the other end of the circle
                else
                { xLoc = circle->centerX - x; }

                relativePixelLoc = ( yLoc * bitmap->width ) + xLoc;

                // Go to first 4-bytes in memory representing our bitmap, then go to disired pixel
                pixel = firstPixel + relativePixelLoc;

                if( OutOfMemoryBounds( bitmap, pixel ) )
                { continue; }

                ColorPixel( pixel, color );
            }
        }
        // Find the next point on half circle
        NextHalfCirclePoint( circle->radius, x, y, &tempX, &tempY );
        x = tempX;
        y = tempY;
    }
}
/*
// Used to step through our bytestream
// Will move to desired byte, cast that byte to desired type, and then derefrence that byte
// We end with incrementing the offset so that we may call this function repeatedly
// Incrementing the offset will allow us to step over what we just accessed in the byte stream
#define READ_BYTESTREAM( type, ptr, off ) (*(type*)(ptr + off)); off += sizeof(type);

// Loads a bitmap into memory
// TODO: This version mimics Jesse Pritchard's code due to my current ignorance and needs redone
static inline void LoadBitmapFromFile( const char* filename, Bitmap* loadedBitmap, const RGB* color )
{
    int returncode = 0; // Return value
    HANDLE file; // Create a (Windows) Handle to our file
    char* pixelbuf = NULL; // ASSUMPTION


}
*/

















//
