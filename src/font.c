

#include "font.h"



HFONT CreateFontWrapper( int height, int width )
{
    return CreateFont( height, width, 0, 0, FW_DONTCARE, 0, 0, 0, BALTIC_CHARSET, OUT_DEFAULT_PRECIS,
                       CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, FF_DONTCARE, 0);
}

void SelectFontWrapper( HDC deviceContext, HFONT font )
{
    SelectFont( deviceContext, font );
}

int DrawTextWrapper( HDC deviceContext, char* string, Rect loc )
{
    RECT rect = { loc.x, loc.y, loc.x + loc.w, loc.y + loc.h};
    LPCTSTR text = (LPCSTR) string;

    DrawText( deviceContext, text, -1, &rect, DT_CENTER );

    return 1;
}
