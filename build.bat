
@echo off

cd obj

gcc -c -I../inc ../src/main.c ../src/appstate.c ../src/collection.c ../src/font.c ../src/pixel.c

cd..

gcc -D DEBUG obj/main.o obj/appstate.o obj/collection.o obj/font.o obj/pixel.o -o EE -lgdi32
