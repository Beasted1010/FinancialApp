

// File used for Collections
// Collections are entities that contain Modules
// Modules are containers of controls, such as buttons and textboxes
// Modules will be defined in here as well
#ifndef COLLECTION_H
#define COLLECTION_H



typedef struct CollectionGridStruct
{
    int cellW;
    int cellH;

    int gridW;
    int gridH;

    int numCells;
} CollGrid;











void InitializeGrid( int cellWidth, int cellHeight, int gridWidth, int gridHeight );

#endif // COLLECTION_H
