

// File used for bitmap information and rendering
#ifndef PIXEL_H
#define PIXEL_H

#include <windows.h>
#include <stdint.h>

typedef enum BitmapTypeEnum
{
    COLOR, // Default -> 0
    MONOCHROME
} BitmapType;

typedef enum ClearColorEnum
{
    // NOTE: BLACKNESS/WHITENESS NOT ALLOWED DUE TO THEM BEING DEFINED IN windows.h
    BLACK, // DEFAULT -> = 0
    WHITE
} ClearColor;

typedef struct RGBStruct
{
    // Red, Green, Blue
    uint8_t r, g, b;
} RGB;

// TODO: Bitmap structure currently uses Windows Dependent BITMAPINFO struct
// This BitmapInfo struct will replace the Windows Dependent structure
// Only need RGBQUAD bmiColors info thing adjusted
typedef struct BitmapInfoHeaderStruct
{
    unsigned long size;
    long width;
    long height;
    unsigned short planes;
    unsigned short bitCount;
    unsigned long compression;
    unsigned long sizeImage;
    long xPelsPerMeter;
    long yPelsPerMeter;
    unsigned long clrUsed;
    unsigned long clrImportant;
} BitmapInfoHeader;
// TODO: See above TODO
typedef struct BitmapInfoStruct
{
    BitmapInfoHeader bmiHeader;
    RGB colors;
} BitmapInfo;

typedef struct BitmapStruct
{
    BITMAPINFO info;
    void* memory;
    int memorySpan;
    int width;
    int height;
    uint8_t bytesPerPixel; // Perhaps bloat
    uint16_t pitch; // Perhaps bloat
    BitmapType bitmapType;
    ClearColor clearColor;
} Bitmap;

typedef struct LineStruct
{
    uint16_t startX, startY, endX, endY;
} Line;

typedef struct RectStruct
{
    uint16_t x, y, w, h;
} Rect;

typedef struct CircleStruct
{
    uint16_t centerX, centerY;
    uint16_t radius;
} Circle;



void InitializeBitmap( Bitmap* bitmap, const uint16_t* width,
                        const uint16_t* height, const uint8_t* clearColor );
void DestroyBitmap( Bitmap* bitmap );
void DrawLine( const Bitmap* bitmap, Line* line, const RGB* color );
void DrawRectangle( const Bitmap* bitmap, const Rect* rect, const RGB* color );
void FillRectangle( const Bitmap* bitmap, const Rect* rect, const RGB* color );
void DrawCircle( const Bitmap* bitmap, const Circle* circle, const RGB* color );




#endif // PIXEL_H
