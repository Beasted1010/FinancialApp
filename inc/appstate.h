

// File used to provide logic behind the current state of the application
#ifndef APPSTATE_H
#define APPSTATE_H

#include <stdint.h>

typedef struct WindowDimensionsStruct
{
    uint16_t width;
    uint16_t height;
    uint16_t minWidth;
    uint16_t minHeight;
} WindowDimensions;


typedef struct AppStateStruct
{
    WindowDimensions windowDimensions;
    uint8_t running; // A yes/no value to serve as logic for app loop
} AppState;


void InitializeAppState( AppState* state, uint16_t minWndSize, uint16_t maxWndSize );


#endif // APPSTATE_H
