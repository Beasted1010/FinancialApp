

// File used for font management
#ifndef FONT_H
#define FONT_H

#include "windows.h"
#include "windowsx.h"
#include "pixel.h"

typedef struct FontStruct
{

} Font;



HFONT CreateFontWrapper( int height, int width );
void SelectFontWrapper( HDC deviceContext, HFONT font );
int DrawTextWrapper( HDC deviceContext, char* string, Rect rect );



#endif // FONT_H
